import SignUp from './pages/SignUp'

function App() {
  return (
    <main className="w-full h-auto flex justify-center items-center py-4">
      <SignUp />
    </main>
  )
}

export default App
