import { cn } from '@/lib/utils'
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle
} from '../components/ui/card'
import SignUpForm from '@/components/signup_form'

type CardProps = React.ComponentProps<typeof Card>
export default function SignUp({ className, ...props }: CardProps) {
  return (
    <Card className={cn('w-[380px]', className)} {...props}>
      <CardHeader>
        <CardTitle>Sign Up</CardTitle>
        <CardDescription>Create a new account</CardDescription>
      </CardHeader>
      <CardContent className="grid gap-4">
        <SignUpForm />
      </CardContent>
      <CardFooter className="flex gap-2">
        <p>Already have an account?</p>
        <a className="text-blue-500 cursor-pointer" href="#">
          Log in
        </a>
      </CardFooter>
    </Card>
  )
}
