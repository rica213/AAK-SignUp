import { useState } from 'react'
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue
} from '@/components/ui/select'

const UserType = () => {
  const [userType, setUserType] = useState('researcher')

  const handleUserTypeChange = (newValue: string) => {
    setUserType(newValue)
  }

  return (
    /**
     * TODO: Fix Bug - Value does not updated after being selected
     */
    <Select defaultValue={userType} onValueChange={handleUserTypeChange}>
      <SelectTrigger className="w-[180px]">
        <SelectValue placeholder="Select your role" />
      </SelectTrigger>
      <SelectContent>
        <SelectItem value="researcher">Researcher</SelectItem>
        <SelectItem value="investor">Investor</SelectItem>
        <SelectItem value="institution_staff">Institution Staff</SelectItem>
        <SelectItem value="service_provider">Service Provider</SelectItem>
      </SelectContent>
    </Select>
  )
}

export default UserType
