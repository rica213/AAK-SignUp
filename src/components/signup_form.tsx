import axios from 'axios'
import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import { z } from 'zod'
import { registrationSchema } from '../../schema/index'
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage
} from '@/components/ui/form'
import { Input } from '@/components/ui/input'
import { Button } from '@/components/ui/button'
import UserType from './ui/user_type'

interface FormFieldProps {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  control: any
  name: string
  label: string
  type: string
}

const generateFormField = ({ control, name, label, type }: FormFieldProps) => (
  <FormField
    control={control}
    name={name}
    render={({ field }) => (
      <FormItem>
        <FormLabel>{label}</FormLabel>
        <FormControl>
          <Input placeholder={`Enter ${label}`} {...field} type={type} />
        </FormControl>
        <FormMessage />
      </FormItem>
    )}
  />
)

const SignUpForm = () => {
  const form = useForm<z.infer<typeof registrationSchema>>({
    resolver: zodResolver(registrationSchema),
    defaultValues: {
      user_type: 'researcher',
      first_name: '',
      last_name: '',
      username: '',
      email: '',
      password: ''
    }
  })

  function onSubmit(values: z.infer<typeof registrationSchema>) {
    /* eslint-disable @typescript-eslint/no-unused-vars */
    const { confirm_password, ...formData } = values
    axios
      .post('https://django-dev.aakscience.com/signup/', formData)
      .then(response => {
        console.log('Registration successful:', response.data)
      })
      .catch(error => {
        console.error('Registration failed:', error)
      })
  }
  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-2">
        <FormField
          control={form.control}
          name="user_type"
          render={() => (
            <FormItem>
              <FormLabel>User Type</FormLabel>
              <FormControl>
                <UserType />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        {generateFormField({
          control: form.control,
          name: 'first_name',
          label: 'First Name',
          type: 'text'
        })}
        {generateFormField({
          control: form.control,
          name: 'last_name',
          label: 'Last Name',
          type: 'text'
        })}
        {generateFormField({
          control: form.control,
          name: 'username',
          label: 'Username',
          type: 'text'
        })}
        {generateFormField({
          control: form.control,
          name: 'email',
          label: 'Email',
          type: 'email'
        })}
        {generateFormField({
          control: form.control,
          name: 'password',
          label: 'Password',
          type: 'password'
        })}
        {generateFormField({
          control: form.control,
          name: 'confirm_password',
          label: 'Confirm Password',
          type: 'password'
        })}
        <Button className="w-full">Register</Button>
      </form>
    </Form>
  )
}

export default SignUpForm
