import { z } from 'zod'

export const registrationSchema = z
  .object({
    user_type: z.enum(
      ['researcher', 'investor', 'institution_staff', 'service_provider'],
      { message: 'User Type cannot be empty.' }
    ),
    first_name: z
      .string()
      .min(1, { message: 'The first name should not be empty.' }),
    last_name: z
      .string()
      .min(1, { message: 'The last name should not be empty.' }),
    username: z
      .string()
      .min(4, { message: 'The username must have at least 4 characters.' })
      .max(50, { message: 'The username is too long.' }),
    email: z.string().email({ message: 'Please provide a valid email.' }),
    password: z
      .string()
      .min(10, { message: 'Password must be at least 10 characters.' }),
    confirm_password: z.string()
  })
  .refine(data => data.password === data.confirm_password, {
    message: 'Passwords do not match',
    path: ['confirm_password']
  })
