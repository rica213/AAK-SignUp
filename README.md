# AAK SIGN UP PAGE

This repo provides a signup page connecting to a Django API endpoint. <br> The
form is built with React Hook Form and Zod for the validation.

## FEATURES

- Well-structured form and semantically correct.
- Accessible with ARIA attributes and proper labels.
- Well validated

## FUTURE FEATURES

- Automated Testing
